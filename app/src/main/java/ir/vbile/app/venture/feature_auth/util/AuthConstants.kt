package ir.vbile.app.venture.feature_auth.util

object AuthConstants {
    const val SPLASH_SCREEN_DURATION = 2000L
    const val MIN_USERNAME_LENGTH = 3
    const val MIN_PASSWORD_LENGTH = 5
}