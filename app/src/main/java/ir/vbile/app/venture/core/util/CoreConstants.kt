package ir.vbile.app.venture.core.util

object CoreConstants {
    const val KEY_JWT_TOKEN = "jwt_token"
    const val SHARE_PREFERENCES_NAME = "share_preferences_name"
    const val MAX_POST_DESCRIPTION_LINES = 3
}