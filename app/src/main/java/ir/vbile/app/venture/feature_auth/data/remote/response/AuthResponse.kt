package ir.vbile.app.venture.feature_auth.data.remote.response


data class AuthResponse(
    val token : String
)
