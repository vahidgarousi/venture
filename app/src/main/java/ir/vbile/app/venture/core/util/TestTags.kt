package ir.vbile.app.venture.core.util

object TestTags {
    const val STANDARD_TEXT_FIELD = "standard_text_field"
    const val EMAIL_STANDARD_TEXT_FIELD = "email_standard_text_field"
    const val PASSWORD_STANDARD_TEXT_FIELD = "password_standard_text_field"
    const val PASSWORD_TOGGLE = "password_toggle"
    const val SPLASH_SCREEN_LOGO = "splash_screen_logo"
    const val BUTTON_LOGIN = "button_login"
    const val LOGIN_SCREEN_BOX = "login_screen_box"
    const val ERROR_TAG = "error_test_tag"
    const val ERROR_EMAIL_TAG = "error_test_tag"
    const val ERROR_PASSWORD_TAG = "error_password_tag"
    const val BUTTON_SIGN_UP = "button_sign_up"
}