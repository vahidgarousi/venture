package ir.vbile.app.venture.feature_auth.presentation.login

data class LoginState(
    val isLoading: Boolean = false
)
