package ir.vbile.app.venture.core.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val ColdSteal = Color(0XFF262435)
val BlackVelvet= Color(0XFF242230)
val ApplausePlease= Color(0XFF838C9C)
val IrrigoPurple= Color(0XFF9D47FF)
val TextWhite= Color(0xFFFFFFFF)
val Charade= Color(0xFF2E2C3B)
val SteelGray = Color(0XFF1A1825)
val HintGray = Color(0xFF6D6D6D)